package exam;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
public class Exercise2 {
}

class Editor extends JFrame implements ActionListener {

    JButton button;
    JTextField textField, textField1, textField2;

    Editor() {

        textField = new JTextField();
        textField.setBounds(90, 50, 150, 30);
        add(textField);

        textField1 = new JTextField();
        textField1.setBounds(90, 80, 150, 30);
        add(textField1);

        textField2 = new JTextField("Result ");
        textField2.setBounds(90, 140, 150, 30);
        textField2.setEditable(false);
        add(textField2);

        button = new JButton("*");
        button.setBounds(90, 200, 100, 30);
        add(button);

        button.addActionListener(this);

        setLayout(null);
        setSize(600, 400);
        setVisible(true);

    }

    public void actionPerformed(ActionEvent e) {

        int a = Integer.parseInt(textField.getText());
        int b = Integer.parseInt(textField1.getText());
        int c = 1;

        if (e.getSource().equals(button)) {
            c = a * b;
            textField2.setText(String.valueOf(c));

        }
    }

    public static void main(String args[]) {
        new Editor();
    }
}
